import './App.css';
import {Container,Button} from 'reactstrap';
function App() {
  return (
    <>
    <Container className="home">
     <h1><b>VRT36</b></h1>
     <h4>I make EDM tracks to make other people happy :)
    <br/><br/>
     </h4>
     <hr style={{borderWidth:'3px',borderStyle:'inset',borderColor:'#276de3',backgroundColor:'#276de3',Color:'#276de3'}}/>
     <h4>#DecentralizeTheWeb</h4>
     <a href='https://chat.vrt36.com' target="_blank" rel="noreferrer"><Button className='vrt' outline>chat.vrt36.com</Button></a><br/><br/>
    </Container>
    </>
  );
}

export default App;